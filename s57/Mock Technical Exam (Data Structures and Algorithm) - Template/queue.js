let collection = [];

// Write the queue functions below.
//1


function print(){
    return collection;
}


function enqueue(x) {
  collection.push(x);

  return collection;
}


//3
function dequeue() {


  collection.shift();

  return collection;
}

//4
function front() {
  return collection[0];
}

//5
function size() {
  return collection.length;
}

//6
function isEmpty() {
   return collection.length === 0;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};